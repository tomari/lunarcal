/*
    Calendar with moon phases
    Copyright (C) 2013  Hisanobu Tomari

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "phases.h"

static time_t timeByMoonphase(unsigned int lun, int phi);
static void lunationEdgesOfTime(const time_t t,
	unsigned int *lower_lun,int *lower_phi,time_t *lower_time,
	unsigned int *upper_lun,int *upper_phi,time_t *upper_time);
static time_t lunphi_next(const unsigned int lun, const int phi,
	unsigned int *next_lun, int * next_phi);
static void fillCalendar(const int year, const int month, double *lunations, int *quarterDay);
static void prettyPrint(FILE *out, const int year, const int month, const double *lunations, const int *quarterDay);
static void usage(void);
extern int main(int argc, char *argv[]);


static time_t timeByMoonphase(unsigned int lun, int phi) {
	double JDE=moonphasebylunation(lun,phi);
	time_t res=JDtoDate(JDE,0);
	return res;
}

static void lunationEdgesOfTime(const time_t t,
				unsigned int *lower_lun,int *lower_phi,time_t *lower_time,
				unsigned int *upper_lun,int *upper_phi,time_t *upper_time) {
	unsigned int lunphi_lower=0, lunphi_upper, lunphi_mid;
	time_t time_mid,time_upper=t,time_lower=t;
	lunphi_upper=(sizeof(time_t)<8)?5691:16777216; /* time_t still 32-bit long */
	do {
		lunphi_mid=(lunphi_lower+lunphi_upper)>>1;
		time_mid=timeByMoonphase((lunphi_mid>>2),(lunphi_mid&3));
		if(time_mid>=t) {
			lunphi_upper=lunphi_mid;
			time_upper=time_mid;
		} else {
			lunphi_lower=lunphi_mid;
			time_lower=time_mid;
		}
	} while (lunphi_upper-lunphi_lower>1);
	*lower_lun=(lunphi_lower>>2);
	*lower_phi=(lunphi_lower&3);
	*upper_lun=(lunphi_upper>>2);
	*upper_phi=(lunphi_upper&3);
	*lower_time=time_lower;
	*upper_time=time_upper;
}

static time_t lunphi_next(const unsigned int lun, const int phi,
			  unsigned int *next_lun, int * next_phi) {
	unsigned int lunphi_next=((lun<<2)|(phi&3))+1;
	*next_lun=(lunphi_next>>2);
	*next_phi=lunphi_next&3;
	return timeByMoonphase(*next_lun,*next_phi);
}

static void fillCalendar(const int year, const int month, double *lunations, int *quarterDay) {
	struct tm man_readable_time;
	time_t mac_time,lower_t,upper_t;
	unsigned int lower_lun,upper_lun;
	int lower_phi,upper_phi;
	/* char timebuf[30];*/ /* debug */
	int fillingDay=0;

	memset(&man_readable_time,0,sizeof(man_readable_time));
	man_readable_time.tm_mday=1;
	man_readable_time.tm_mon=month-1;
	man_readable_time.tm_year=year-1900;
	mac_time=mktime(&man_readable_time);
	lunationEdgesOfTime(mac_time,
			    &lower_lun,&lower_phi,&lower_t,
			    &upper_lun,&upper_phi,&upper_t);
	/*fprintf(stderr,"target day= %s",ctime_r(&mac_time,timebuf));
	fprintf(stderr,"LowerLun= %d LowerPhi=%d %s",lower_lun,lower_phi,ctime_r(&lower_t,timebuf));
	fprintf(stderr,"UpperLun= %d UpperPhi=%d %s",upper_lun,upper_phi,ctime_r(&upper_t,timebuf));*/

	do {
		double diff_x,diff_y,diff_z;
		double lunation_r,lunation_fraction;
		diff_x=difftime(upper_t,lower_t);
		diff_y=difftime(mac_time,lower_t);
		lunation_fraction=diff_y/diff_x;
		if(lunation_fraction<1.) {
			diff_z=difftime(upper_t,mac_time);
			lunation_r=lunation_fraction+(double)lower_phi;
			quarterDay[fillingDay]=(diff_z<86400.)?(double)upper_phi:-1;
			lunations[fillingDay]=lunation_r;
			/*fprintf(stderr,"day= %d lunation= %f\n",fillingDay+1,lunation_r);*/
			fillingDay++;
			man_readable_time.tm_mday++;
			mac_time=mktime(&man_readable_time);
		} else {
			lower_lun=upper_lun;
			lower_phi=upper_phi;
			lower_t=upper_t;
			upper_t=lunphi_next(lower_lun,lower_phi,
					    &upper_lun,&upper_phi);
		}
	} while(man_readable_time.tm_mon==(month-1));
}

static void prettyPrint_WeekJustify(FILE *o, int wday) {
	unsigned i;
	for(i=0; i<wday; i++) { fputs("       ",o); }
}

static void prettyPrintWeek_Dates(FILE *o, struct tm* startDay) {
	int thismon=startDay->tm_mon;
	time_t today;
	struct tm ltime;
	unsigned int labelidx;
	static const char daylabel[2][9]={"   %2d  ","===%2d= "};
	time(&today);
	localtime_r(&today,&ltime);
	prettyPrint_WeekJustify(o,startDay->tm_wday);
	do {
		if(ltime.tm_year==startDay->tm_year &&
				ltime.tm_mon==startDay->tm_mon &&
				ltime.tm_mday==startDay->tm_mday) {
			labelidx=1;
		} else {
			labelidx=0;
		}
		fprintf(o,daylabel[labelidx],startDay->tm_mday);
		startDay->tm_mday++;
		mktime(startDay);
	} while((startDay->tm_wday) && thismon==startDay->tm_mon);
	fputc('\n',o);
}

static const char pLabel[8][8]={
		"[    ] ","[   *] ","[  **] ","[ ***] ",
		"[****] ","[*** ] ","[**  ] ","[*   ] "};

static void prettyPrintWeek_Phases(FILE *o, struct tm* startDay, const double *lun, const int *qinfo) {
	int thismon=startDay->tm_mon;
	prettyPrint_WeekJustify(o,startDay->tm_wday);
	do {
		if(qinfo[startDay->tm_mday-1]<0) {
			int idx=(int) (2.*floor(lun[startDay->tm_mday-1])+1.); /* yay, fma */
			fputs(pLabel[idx],o);
		} else {
			fputs(pLabel[2*qinfo[(startDay->tm_mday-1)]],o);
		}
		startDay->tm_mday++;
		mktime(startDay);
	} while((startDay->tm_wday) && thismon==startDay->tm_mon);
	fputc('\n',o);
}

static void prettyPrint(FILE *out, const int year, const int month,
			const double *lunations, const int *quarterDay) {
	static const char *monthname[12]={"January", "February", "March", "April",
					"May", "June", "July", "August",
					"September", "October", "November", "December"};
	struct tm tmbuf;
	memset(&tmbuf,0,sizeof(tmbuf));
	tmbuf.tm_mday=1;
	tmbuf.tm_mon=month-1;
	tmbuf.tm_year=year-1900;
	mktime(&tmbuf); /* calc day-of-week */
	fprintf(out,"\n"
		    "  %s %d\n\n"
		    "  Sun    Mon    Tue    Wed    Thu    Fri    Sat  \n\n",
		monthname[tmbuf.tm_mon],tmbuf.tm_year+1900);
	/*  A day -> ___12__
	             [*   ]_
	             _______ */
	while(tmbuf.tm_mon==(month-1)) {
		struct tm weekstart;
		weekstart=tmbuf;
		prettyPrintWeek_Dates(out,&tmbuf);
		prettyPrintWeek_Phases(out,&weekstart,lunations,quarterDay);
		fputc('\n',out);
	}
}

static void usage() {
	fputs("usage: lunarcal <month> <year>\n",stderr);
	exit(EXIT_FAILURE);
}

extern int main(int argc, char *argv[]) {
	/*time_t now;
	  now=time(NULL);*/
	unsigned int year,mon;
	double lunations[50]; /* maybe 31 would be sufficient */
	int quarters[50];
	struct tm tmnow;
	time_t now;
	time(&now);
	localtime_r(&now,&tmnow);
	year=tmnow.tm_year+1900;
	mon=tmnow.tm_mon+1;
	if(argc>1) { mon=(int)strtol(argv[1],NULL,10); }
	if(argc>2) { year=(int)strtol(argv[2],NULL,10); }
	if(mon<1 || mon>12) { usage(); }
	fillCalendar(year,mon,lunations,quarters);
	prettyPrint(stdout,year,mon,lunations,quarters);
	exit(EXIT_SUCCESS);
	return EXIT_FAILURE;
}
